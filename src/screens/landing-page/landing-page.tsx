import React, { useEffect, useState } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Box, Text, Spacing, Button, Stack } from '../../components/main';
import { useNavigation } from '@react-navigation/core';
import { NavigationRoutes } from '../../navigation/nav-params';
import auth from '@react-native-firebase/auth'
import { useCollection } from '../../hooks';
import { SignIn } from '../sign-in-screen/sign-in';
import { SignUp } from '../sign-up-screen/sign-up';

export const LandingPage = () => {
    const navigation = useNavigation()
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [signedIn, setSignedIn] = useState(true);
    const users = useCollection('users');

    const signUp = () => {
        if (email && password) {
            auth().createUserWithEmailAndPassword(email, password)
                .then(async ({ user }: any) => {
                    await users.updateRecord(user.uid, { uid: user.uid, name: '' });
                    navigation.navigate(NavigationRoutes.MainRoot);
                });
        }
    };

    const signIn = () => {
        if (email && password)
            auth().signInWithEmailAndPassword(email, password)
                .then(() => {
                    navigation.navigate(NavigationRoutes.MainRoot);
                });
    }

    return (
        <SafeAreaView style={{ backgroundColor: '#5856D6', flex: 1 }}>
            <Spacing pad={5}>
                <Box width={'100%'} height={'100%'} justifyContent="center">
                    <Stack size={1}>
                        <Text type={'largeTitle'} bold role={'white'} textAlign={'center'}>Welcome to the Shopping List App!</Text>
                    </Stack>
                    {
                        signedIn ?
                            <SignIn setEmail={setEmail} setPassword={setPassword} /> :
                            <SignUp setEmail={setEmail} setPassword={setPassword} />
                    }
                    <Spacing marginbot={20}>
                        <Button width={'100%'} type={'secondary'} onPress={() => signedIn ? signIn() : signUp()}>
                            <Text type={'title2'} bold weight={'600'} role={'white'}>{signedIn ? 'Sign In' : 'Sign Up'}</Text>
                        </Button>
                    </Spacing>
                    <Stack size={2}>
                        <Text type={'title2'} bold role={'white'} textAlign={'center'}>Save Your Time & Energy</Text>
                        <Spacing margintop={3} marginbot={20}>
                            <Text type={'headline'} role={'white'} textAlign={'center'}>Make shopping smarter, avoid extra trips to the supermarkets & malls and get more organized using the Shopping List App.</Text>
                        </Spacing>
                    </Stack>
                    <Box flexDirection={'row'} justifyContent={'space-evenly'}>
                        <Button type={'secondary'} size='large' width={'40%'} selected={signedIn ? true : false} onPress={() => { setSignedIn(false) }}>
                            <Text type={'headline'} bold role={'white'}>Sign Up</Text>
                        </Button>
                        <Button type={'secondary'} size='large' width={'40%'} selected={signedIn ? false : true} onPress={() => { setSignedIn(true) }}>
                            <Text type={'headline'} bold role={'white'}>Sign In</Text>
                        </Button>
                    </Box>
                </Box>
            </Spacing>
        </SafeAreaView>
    )
}