import React, { useState, useEffect } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Text, Spacing, Stack } from '../../components/main';
import { ListItem, AddList } from '../../components'
import { FlatList, Alert } from 'react-native';

export const ListScreen = () => {

    const [items, setItems] = useState([
        { text: 'banana', key: 1 }
    ])

    const deleteItem = (key) => {
        setItems(prevItems => {
            return prevItems.filter(item => item.key != key);
        });
    };

    const handleAddList = (text) => {
        if (!text) {
            Alert.alert(
                'Invalid item input',
                'Please do not leave the field empty',
                [
                    {
                        text: 'Ok',
                        style: 'cancel'
                    },
                ],
                { cancelable: true },
            );
        } else {
            setItems(prevItems => {
                return [{ key: Math.random(), text }, ...prevItems];
            });
        }
    };

    return (
        <SafeAreaView style={{ backgroundColor: 'white', flex: 1 }}>
            <Spacing pad={5}>
                <Stack size={2}>
                    <Text type={'largeTitle'} bold>Lists</Text>
                </Stack>
                <Stack size={2}>
                    <AddList handleAddList={handleAddList} />
                </Stack>
                <Spacing margintop={3}>
                    <Stack size={3}>
                        <FlatList
                            data={items}
                            renderItem={({ item }) => <ListItem item={item} deleteItem={deleteItem} />}
                        />
                    </Stack>
                </Spacing>
            </Spacing>
        </SafeAreaView>
    )
}