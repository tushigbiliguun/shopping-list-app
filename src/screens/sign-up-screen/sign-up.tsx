import React, { useState, useContext } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Box, Text, Spacing, Button, Input } from '../../components/main';

interface Props {
    setEmail: Function
    setPassword: Function
}

export const SignUp: React.FC<Props> = ({ setEmail, setPassword }) => {

    return (
        <SafeAreaView style={{ backgroundColor: '#5856D6', flex: 1 }}>
            <Box width={'100%'} height={'100%'} alignItems={'center'}>
                <Box>
                    <Text type={'subheading'} bold role={'white'} textAlign={'center'}>Sign Up for an Account</Text>
                </Box>
                <Spacing margintop={4}>
                <Box flex={1} flexDirection={'column'} alignItems={'center'}>
                    <Input placeholder={'Email'} onChangeText={(value: any) => setEmail(value)} />
                    <Spacing margintop={5}>
                        <Input placeholder={'Password'} type={'password'} onChangeText={(value: any) => setPassword(value)} />
                    </Spacing>
                </Box>
                </Spacing>
            </Box>
        </SafeAreaView>
    )
}