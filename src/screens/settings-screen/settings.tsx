import React from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Box, Text, Spacing, Button } from '../../components/main';

export const SettingsScreen = () => {
    return (
        <SafeAreaView style={{ backgroundColor: 'white', flex: 1 }}>
            <Spacing pad={5}>
                <Box>
                    <Text type={'largeTitle'} bold>Settings</Text>
                </Box>
            </Spacing>
        </SafeAreaView>
    )
}