import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import MainBotNav from './main-bot-nav';
import { NavigationRoutes, NavigatorParamList } from './nav-params';
import { SignIn, SignUp, LandingPage } from '../screens/'
const RootStack = createStackNavigator<NavigatorParamList>();

export const RootNavigationContainer = () => {
    return (
        <NavigationContainer>
            <RootStack.Navigator>
                <RootStack.Screen
                    name={NavigationRoutes.LandingPage}
                    component={LandingPage}
                    options={{ headerShown: false }}
                />
                <RootStack.Screen
                    name={NavigationRoutes.SignUp}
                    component={SignUp}
                    options={{ headerShown: false }}
                />
                <RootStack.Screen
                    name={NavigationRoutes.SignIn}
                    component={SignIn}
                    options={{ headerShown: false }}
                />
                <RootStack.Screen
                    name={NavigationRoutes.MainRoot}
                    component={MainBotNav}
                    options={{ headerShown: false }}
                />
            </RootStack.Navigator>
        </NavigationContainer>
    );
};
