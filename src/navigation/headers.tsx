import React from 'react';
import {
    Spacing,
    Button,
    Text,
} from '../components/main';

export const HeaderLeft: React.FC<any> = ({ onPress }) => {

    return (
        <Button category={'text'} onPress={onPress}>
            <Spacing marginleft={4}>
                <Text role={'primary1'} bold>
                    Cancel
                </Text>
            </Spacing>
        </Button>
    )
};

export const HeaderTitle: React.FC<any> = ({ title = 'test' }) => (
    <Text
        fontFamily={'Montserrat'}
        type={'headline'}
        role={'black'}
        bold
        width={'auto'}
    >
        {title}
    </Text>
);

export const HeaderRight: React.FC<any> = ({ onPress }) => {

    return (
        <Button category={'text'} onPress={onPress}>
            <Spacing marginright={4}>
                <Text role={'primary1'} bold>
                    Done
                </Text>
            </Spacing>
        </Button>
    )
};
