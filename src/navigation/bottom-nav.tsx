import React, { useRef } from 'react';
import { TouchableOpacity, Animated, Dimensions } from 'react-native';
import { Box, Text } from '../components/main';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { ListIcon, SettingsIcon } from '../components/icons';

const tabIcons: any = {
    List: ListIcon,
    Settings: SettingsIcon,
};

const tabNames: any = {
    List: 'Lists',
    Settings: 'Settings',
};

export const BottomTab: React.FC<any> = ({
    state,
    descriptors,
    navigation,
}) => {
    const focusedOptions = descriptors[state.routes[state.index].key].options;
    const xVal = useRef(new Animated.Value(0)).current;

    if (focusedOptions.tabBarVisible === false) {
        return null;
    }

    const insets = useSafeAreaInsets();

    return (
        <Box
            flexDirection="row"
            width="100%"
            height={65 + insets.bottom}
            position="relative"
        >
            {state.routes.map((route: any, index: any) => {
                const CustomIcon = tabIcons[route.name];
                const { options } = descriptors[route.key];
                const label =
                    options.tabBarLabel !== undefined
                        ? options.tabBarLabel
                        : options.title !== undefined
                            ? options.title
                            : route.name;

                const isFocused = state.index === index;

                const onPress = () => {
                    const event = navigation.emit({
                        type: 'tabPress',
                        target: route.key,
                        canPreventDefault: true,
                    });

                    if (!isFocused && !event.defaultPrevented) {
                        navigation.navigate(route.name);
                    }
                };

                const onLongPress = () => {
                    navigation.emit({
                        type: 'tabLongPress',
                        target: route.key,
                    });
                };

                return (
                    <TouchableOpacity
                        accessibilityRole="button"
                        key={route.name}
                        accessibilityState={isFocused ? { selected: true } : {}}
                        accessibilityLabel={options.tabBarAccessibilityLabel}
                        testID={options.tabBarTestID}
                        onPress={onPress}
                        onLongPress={onLongPress}
                        style={{
                            flex: 1,
                            height: 65,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                    >
                        <CustomIcon role={isFocused ? 'primary1' : 'gray'} />
                        <Text
                            role={isFocused ? 'primary1' : 'gray'}
                            type="caption2"
                            textAlign="center"
                            fontFamily="Montserrat"
                        >
                            {tabNames[label]}
                        </Text>
                    </TouchableOpacity>
                );
            })}
        </Box>
    );
};
