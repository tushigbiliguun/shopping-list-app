export enum NavigationRoutes {
    MainRoot = 'MainRoot',
    List = 'List',
    Settings = 'Settings',
    SignUp = 'SignUp',
    SignIn = 'SignIn',
    LandingPage = 'LandingPage'
}
export interface NavigationPayload<T> {
    props: T;
}
export type NavigatorParamList = {
    [NavigationRoutes.List]: NavigationPayload<any>;
    [NavigationRoutes.Settings]: NavigationPayload<any>;
    [NavigationRoutes.SignUp]: NavigationPayload<any>;
    [NavigationRoutes.SignIn]: NavigationPayload<any>;
    [NavigationRoutes.LandingPage]: NavigationPayload<any>;
};
