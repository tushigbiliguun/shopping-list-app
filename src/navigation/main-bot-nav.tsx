import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { BottomTab } from './bottom-nav';
import { ListScreen, SettingsScreen } from '../screens';
import { NavigationRoutes } from './nav-params';
const Tab = createBottomTabNavigator();

const MainBotNav = () => {
    return (
        <Tab.Navigator tabBar={(props) => <BottomTab {...props} />}>
            <Tab.Screen name={NavigationRoutes.List} component={ListScreen} />
            <Tab.Screen name={NavigationRoutes.Settings} component={SettingsScreen} />
        </Tab.Navigator>
    );
};

export default MainBotNav;
