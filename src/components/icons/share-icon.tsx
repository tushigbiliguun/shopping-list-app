import * as React from 'react';
import Svg, { Path } from "react-native-svg"
import { useTheme } from '../theme-provider';
import {IconType} from '../type'

export const ShareIcon: React.FC<IconType> = ({
  width = 16,
  height = 24,
  role = 'white'
}) => {

  const { colors } = useTheme()

  return (
    <Svg width={width} height={height} viewBox="0 0 16 24" fill="none">
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8.343 1.021l3.513 3.57a.64.64 0 010 .892.605.605 0 01-.87 0L8.626 3.085v12.787c0 .349-.277.63-.616.63a.624.624 0 01-.617-.63V3.115L5.06 5.483a.62.62 0 01-.87.001.64.64 0 010-.893l3.514-3.57c.203-.206.44-.205.64 0zm7.076 8.93v11.836a1.48 1.48 0 01-1.479 1.48H2.106a1.48 1.48 0 01-1.48-1.48V9.95c0-.818.663-1.48 1.48-1.48h3.161V9.74H2.67a.774.774 0 00-.774.774v10.711c0 .427.346.775.774.775h10.708a.775.775 0 00.774-.775v-10.71a.774.774 0 00-.774-.775H10.77V8.47h3.17c.817 0 1.48.662 1.48 1.48z"
        fill={colors[role]}
      />
    </Svg>
  )
}
