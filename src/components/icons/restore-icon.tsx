import * as React from 'react';
import Svg, { Path } from "react-native-svg"
import { useTheme } from '../theme-provider';
import {IconType} from '../type'

export const RestoreIcon: React.FC<IconType> = ({
  width = 22,
  height = 26,
  role = 'gray'
}) => {

  const { colors } = useTheme()

  return (
    <Svg width={width} height={height} viewBox="0 0 22 26" fill="none">
      <Path
        d="M20.68 14.794a.816.816 0 00-.815.744c-.377 4.544-4.2 8.088-8.849 8.088-4.894 0-8.88-3.96-8.88-8.827 0-4.79 3.86-8.706 8.65-8.826a.214.214 0 01.225.219v2.75c0 .69.76 1.105 1.345.739L17.617 6a.874.874 0 000-1.483L12.362.887a.877.877 0 00-1.346.738v2.5c0 .12-.093.218-.213.218C5.093 4.458.5 9.096.5 14.8c0 5.775 4.709 10.451 10.516 10.451 5.513 0 10.03-4.194 10.478-9.576a.813.813 0 00-.814-.88z"
        fill={colors[role]}
      />
    </Svg>
  )
}
