import * as React from 'react';
import Svg, { Path } from "react-native-svg"
import { useTheme } from '../theme-provider';
import {IconType} from '../type'

export const TrashIcon: React.FC<IconType> = ({
  width = 16,
  height = 24,
  role = 'gray'
}) => {

  const { colors } = useTheme()

  return (
    <Svg width={width} height={height} viewBox="0 0 16 22" fill="none">
      <Path
        d="M1.279 4l1.29 15.898c0 .886.717 1.602 1.603 1.602h7.71c.887 0 1.603-.716 1.603-1.602L14.76 4H1.28zm3.369 14.492L4.28 5.75h1.012l.377 12.742H4.648zm3.844 0h-.984V5.75h.984v12.742zm2.86 0h-1.017l.372-12.742h1.012l-.367 12.742zm2.554-16.46h-1.969L10.5.772A1.108 1.108 0 009.777.5H6.234c-.268 0-.53.098-.733.273L4.062 2.031H2.095c-.963 0-1.64.46-1.64 1.422h15.093c0-.962-.678-1.422-1.64-1.422z"
        fill={colors[role]}
      />
    </Svg>
  )
}
