import React, { useState } from 'react';
import { Box, Text, Spacing, Input, Stack, Button } from '../components/main';

export const AddList = ({ handleAddList }) => {

    const [text, setText] = useState()

    return (
        <Box role={'white'} width={'100%'}>
            <Spacing padtop={1}>
                <Stack size={1}>
                    <Box flexDirection={'row'} justifyContent={'space-between'}>
                        <Text type={'headline'} bold role={'primary1'}>Add List</Text>
                    </Box>
                </Stack>
                <Stack size={3}>
                    <Spacing margintop={2}>
                        <Box flexDirection={'row'} justifyContent={'space-between'}>
                            <Input type={'default'} placeholder={'Add item'} onChangeText={(text: string) => setText(text)} width={'85%'} value={text} />
                            <Button type={'primary'} onPress={() => { handleAddList(text); setText('') }} category={'fill'} size={'large'}>
                                <Text role={'white'} type={'headline'} bold>Add</Text>
                            </Button>
                        </Box>
                    </Spacing>
                </Stack>
            </Spacing>
        </Box>
    )
}