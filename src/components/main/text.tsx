import React from 'react';
import { Text as RNText, StyleSheet } from 'react-native';
import { useTheme } from '../theme-provider';
import { ColorType } from '../type';
import _ from 'lodash';

const FONT_TYPES = {
    largeTitle: {
        fontSize: 34,
        lineHeight: 41,
        letterSpacing: 0.4,
    },
    title1: {
        fontSize: 28,
        lineHeight: 34,
        letterSpacing: 0.34,
    },
    title2: {
        fontSize: 22,
        lineHeight: 28,
        letterSpacing: 0.35,
    },
    headline: {
        fontSize: 17,
        lineHeight: 22,
        letterSpacing: -0.41,
    },
    body: {
        fontSize: 17,
        lineHeight: 22,
        letterSpacing: -0.41,
    },
    subheading: {
        fontSize: 15,
        lineHeight: 20,
        letterSpacing: -0.24,
    },
    footnote: {
        fontSize: 13,
        lineHeight: 18,
        letterSpacing: -0.08,
    },
    caption1: {
        fontSize: 12,
        lineHeight: 16,
        letterSpacing: 0,
    },
    caption2: {
        fontSize: 11,
        lineHeight: 13,
        letterSpacing: 0.07,
    },
};

export const Text: React.FC<TextType> = ({
    type = 'body',
    role,
    bold,
    height,
    width,
    fontFamily,
    underline,
    textAlign,
    numberOfLines,
    opacity,
    children,
}) => {
    const { colors } = useTheme();
    const styles = StyleSheet.create({
        shape: {
            width: width || 'auto',
            height: height || 'auto',
            textAlign: textAlign || 'left',
            flexShrink: 1,
        },
        text: {
            ...FONT_TYPES[type],
            color: colors[role || 'black'],
            fontWeight: bold ? '700' : '400',
            textDecorationLine: underline ? 'underline' : undefined,
            opacity,
        },
    });

    return (
        <RNText numberOfLines={numberOfLines} style={[styles.shape, styles.text]}>
            {children}
        </RNText>
    );
};

type TextType = {
    type?:
    | 'largeTitle'
    | 'title1'
    | 'title2'
    | 'headline'
    | 'body'
    | 'subheading'
    | 'footnote'
    | 'caption1'
    | 'caption2';
    role?: ColorType;
    bold?: boolean;
    textAlign?: 'center' | 'left' | 'right';
    fontFamily?: 'Montserrat';
    numberOfLines?: number;
    width?: string | number;
    height?: string | number;
    children?: string | JSX.Element | JSX.Element[];
    underline?: boolean;
    opacity?: number;
};
