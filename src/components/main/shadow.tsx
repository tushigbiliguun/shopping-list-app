import React from 'react';
import { View, StyleSheet } from 'react-native';
import { useTheme } from '../theme-provider';
import { ColorType } from '../type';
import _ from 'lodash';

type ShadowType = {
    grow?: number;
    height?: number;
    width?: number;
    radius?: number;
    opacity?: number;
    role?: ColorType;
    children?: string | JSX.Element | JSX.Element[];
};

export const Shadow: React.FC<ShadowType> = ({
    grow = 0,
    role = 'black',
    opacity = 0.1,
    radius = 5,
    height = 0,
    width = 0,
    children,
}) => {
    const { colors } = useTheme();
    const styles = StyleSheet.create({
        boxshadow: {
            flexGrow: grow,
            shadowOpacity: opacity,
            shadowRadius: radius,
            shadowColor: colors[role],
            shadowOffset: { height: height, width: width },
            elevation: 40,
        },
    });

    return <View style={[styles.boxshadow]}>{children}</View>;
};
