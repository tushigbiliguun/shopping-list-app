import React from 'react';
import { Image, StyleSheet, Text } from 'react-native';
import { Box, Border } from './index';

const sizeDimensions = { XL: 100, L: 87, M: 60, S: 28 };
const textDimensions = { XL: 50, L: 34, M: 22, S: 12};

type AvatarType = {
  initial?: string;
  url?: string;
  source?: any;
  size: 'XL' | 'L' | 'M' | 'S';
};

export const Avatar: React.FC<AvatarType> = ({
  initial = 'ДЭ',
  url,
  size,
  source,
}) => {
  const styles = StyleSheet.create({
    image: {
      width: sizeDimensions[size],
      height: sizeDimensions[size],
    },
    text: {
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: textDimensions[size],
    },
  });

  return (
    <Box width={sizeDimensions[size]} height={sizeDimensions[size]}>
      <Border radius={sizeDimensions[size] / 2} role="primary1" lineWidth={4}>
        <Box
          width={sizeDimensions[size] - 8}
          height={sizeDimensions[size] - 8}
          role="primary3"
          justifyContent="center"
          alignItems="center"
        >
          {url ? (
            <Image style={styles.image} source={{ uri: url }} />
          ) : source ? (
            <Image style={styles.image} source={source} />
          ) : (
            <Text style={styles.text}>{initial}</Text>
          )}
        </Box>
      </Border>
    </Box>
  );
};
