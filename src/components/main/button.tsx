import React from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Box, Spacing, Shadow, Border } from './index';

type ButtonType = {
    category?: 'fill' | 'text';
    status?: 'disabled' | 'active' | 'default';
    type?: 'primary' | 'secondary' | 'tertiary';
    size?: 'large' | 'small';
    width?: number | string;
    height?: number | string;
    onPress: Function;
    children?: any;
    selected?: boolean
};

const statusOpacity = {
    default: 1,
    disabled: 0.5,
    active: 0.7,
};

export const Button: React.FC<ButtonType> = ({
    onPress,
    status = 'default',
    type = 'primary',
    size = 'l',
    category = 'fill',
    width,
    children,
    selected
}) => {
    return (
        <Box
            opacity={statusOpacity[status]}
            width={width ? width : 'auto'}
            height={size === 'small' ? 20 : 45}
            alignSelf={'flex-start'}
        >
            <TouchableOpacity onPress={() => onPress()} disabled={status === 'disabled'}>
                <Border
                    radius={4}
                    role={type === 'primary' ? 'primary1' : 'primary2'}
                    lineWidth={category === 'text' ? 0 : 1}
                >
                    <Box
                        role={category === 'fill' ? type === 'primary' ? 'primary1' : 'primary2' : undefined}
                        height={'100%'}
                    >
                        <Shadow
                            role={'black'}
                            radius={0}
                            opacity={0.15}
                            height={category === 'text' ? 0 : 4}
                            width={0}
                        >
                            <Box
                                width={'100%'}
                                height={size === 'small' ? 38 : 44}
                                role={category === 'fill' ? type === 'primary' ? 'primary1' : 'primary2' : undefined}
                                justifyContent={'center'}
                                alignItems={'center'}
                            >
                                <Spacing padright={category === 'text' ? 0 : 2} padleft={category === 'text' ? 0 : 2}>
                                    <Box
                                        justifyContent={'center'}
                                        alignItems={'center'}
                                        alignSelf={'stretch'}
                                    >
                                        {children}
                                    </Box>
                                </Spacing>
                            </Box>
                        </Shadow>
                    </Box>
                </Border>
            </TouchableOpacity>
        </Box>
    );
};
