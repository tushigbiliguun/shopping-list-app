import React from 'react';
import { View, StyleSheet } from 'react-native';
import { useTheme } from '../theme-provider';
import _ from 'lodash';

export const Spacing: React.FC<BoxType> = (props) => {
    const {
        grow,
        margin,
        marginleft,
        marginright,
        margintop,
        marginbot,
        pad,
        padleft,
        padright,
        padtop,
        padbot,
        children,
    } = props;
    const { baseSpace } = useTheme();

    const styles = StyleSheet.create({
        container: {
            flexGrow: grow,
        },
        margin: {
            margin: _.isNumber(margin) ? margin * baseSpace : margin,
            marginLeft: _.isNumber(marginleft) ? marginleft * baseSpace : marginleft,
            marginRight: _.isNumber(marginright) ? marginright * baseSpace : marginright,
            marginTop: _.isNumber(margintop) ? margintop * baseSpace : margintop,
            marginBottom: _.isNumber(marginbot) ? marginbot * baseSpace : marginbot,
        },
        padding: {
            padding: _.isNumber(pad) ? pad * baseSpace : pad,
            paddingLeft: _.isNumber(padleft) ? padleft * baseSpace : padleft,
            paddingRight: _.isNumber(padright) ? padright * baseSpace : padright,
            paddingTop: _.isNumber(padtop) ? padtop * baseSpace : padtop,
            paddingBottom: _.isNumber(padbot) ? padbot * baseSpace : padbot,
        },
    });

    return (
        <View style={[styles.container, styles.margin, styles.padding]}>
            {children}
        </View>
    );
};

type BoxType = {
    grow?: number;
    margin?: string | number;
    marginleft?: string | number;
    marginright?: string | number;
    margintop?: string | number;
    marginbot?: string | number;
    pad?: string | number;
    padleft?: string | number;
    padright?: string | number;
    padtop?: string | number;
    padbot?: string | number;
    children?: JSX.Element | JSX.Element[] | string | any;
};
