import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Spacing } from './spacing';
import { useTheme } from '../theme-provider';
import { ColorType } from '../type';

type StackType = {
    size?: number | string;
    role?: ColorType;
    width?: string | number;
    height?: string | number;
    children?: string | JSX.Element | JSX.Element[] | any;
    justifyContent?:
    | 'center'
    | 'flex-start'
    | 'flex-end'
    | 'space-between'
    | 'space-around'
    | 'space-evenly';
    alignItems?:
    | 'stretch'
    | 'flex-start'
    | 'flex-end'
    | 'center'
    | 'baseline';
};

export const Stack: React.FC<StackType> = (props) => {
    const {
        size = 0,
        role,
        height,
        width,
        children,
        justifyContent,
        alignItems,
    } = props;
    const { colors } = useTheme();

    const styles = StyleSheet.create({
        stack: {
            flexDirection: 'column',
            backgroundColor: role ? colors[role] : 'transparent',
            height,
            width,
            justifyContent: justifyContent,
            alignItems: alignItems,
        },
    });

    return (
        <View style={styles.stack}>
            {React.Children.toArray(children).map((child, index) => {
                if (index == 0) {
                    return <View key={index}>{child}</View>;
                }
                return (
                    <View key={index}>
                        <Spacing margintop={size} />
                        {child}
                    </View>
                );
            })}
        </View>
    );
};
