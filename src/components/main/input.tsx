import React, { useEffect, useRef, useState } from 'react';
import { Animated, StyleSheet, TextInput, View } from 'react-native';
import { Border, Text, Box, Spacing, Stack } from './index';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useTheme } from '../theme-provider';

interface Props {
    placeholder: string | '';
    type?: 'default' | 'password';
    status?: 'default' | 'error' | 'disabled' | 'success';
    opacity?: number
    LeftIcon?: any;
    value?: string;
    RigthIcon?: any;
    counter?: boolean;
    helperText?: string;
    keyboardType?:
    | 'default'
    | 'number-pad'
    | 'decimal-pad'
    | 'numeric'
    | 'email-address'
    | 'phone-pad';
    messageText?: string;
    messageType?: 'default' | 'error' | 'warning' | 'success';
    onKeyPress?: Function;
    onChangeText?: Function;
    onSubmitEditing?: Function;
    width?: any;
    onFocus?: Function;
}

export const InputAllInOne: React.FC<Props> = (props) => {
    const {
        onChangeText,
        helperText,
        counter,
        messageText,
        messageType,
        ...others
    } = props;
    const [value, setValue] = useState('');

    return (
        <Box width={'auto'} height={'auto'}>
            <Stack size={3}>
                <View>
                    <Stack size={1}>
                        <Input
                            onChangeText={(text: string) => {
                                onChangeText && onChangeText(text);
                                setValue(text);
                            }}
                            {...others}
                        />
                    </Stack>
                </View>
            </Stack>
        </Box>
    );
};

export const InputWithMessage: React.FC<Props> = (props) => {

    return (
        <Box width={'100%'}>
            <View>
                <Stack size={1}>
                    <Input {...props} />
                </Stack>
            </View>
        </Box>
    );
};

export const InputWithHelperTextAndCounter: React.FC<Props> = (props) => {
    const { onChangeText, helperText, counter, ...others } = props;
    const [value, setValue] = useState('');

    return (
        <Box width={'100%'} height={'auto'}>
            <Input
                width={'100%'}
                onChangeText={(text: string) => {
                    onChangeText && onChangeText(text);
                    setValue(text);
                }}
                {...others}
            />
        </Box>
    );
};

export const Input: React.FC<Props> = (props) => {
    const { colors } = useTheme();
    const {
        type,
        placeholder,
        status = 'default',
        value,
        keyboardType,
        onKeyPress,
        onChangeText,
        onSubmitEditing,
        width,
        onFocus,
    } = props;
    const [isInputFocus, setIsInputFocus] = useState(false);
    const [visible, setVisible] = useState(true);

    const styles = StyleSheet.create({
        label: {
            position: 'absolute',
            zIndex: 0,
        },

        input: {
            height: 45,
            zIndex: 1,
            marginLeft: 0,
            fontSize: 15,
            fontWeight: '400',
            letterSpacing: 1,
        },
    });

    return (
        <Box
            opacity={status === 'disabled' ? 0.5 : 1}
            height={30}
            width={width ? width : '95%'}
        >
            <Border
                radius={15}
                lineWidth={isInputFocus ? 1 : 1}
                role={
                    status === 'error'
                        ? 'primary2'
                        : status === 'success'
                            ? 'gray'
                            : isInputFocus
                                ? 'primary1'
                                : 'black'
                }
            >
                <Box
                    justifyContent={'center'}
                    role={'white'}
                    height={42}
                    width={'100%'}
                >
                    <Spacing padleft={2} padright={2}>
                        <Box justifyContent={'center'} alignItems={'center'} flexDirection={'row'}>
                            <View style={{ width: '100%' }}>
                                <TextInput
                                    style={[styles.input, { color: colors['black'] }]}
                                    onFocus={() => {
                                        setIsInputFocus(true), onFocus && onFocus();
                                        setVisible(!visible)
                                    }}
                                    onBlur={() => setIsInputFocus(false)}
                                    value={value}
                                    keyboardType={keyboardType}
                                    autoFocus={value ? true : false}
                                    editable={status === 'disabled' ? false : true}
                                    scrollEnabled={false}
                                    secureTextEntry={type === 'password' ? visible : false}
                                    onChangeText={(text) => onChangeText && onChangeText(text)}
                                    onSubmitEditing={() => onSubmitEditing && onSubmitEditing()}
                                    onKeyPress={(e) =>
                                        onKeyPress && onKeyPress(e.nativeEvent.key)
                                    }
                                    placeholder={placeholder}
                                />
                            </View>
                        </Box>
                    </Spacing>
                </Box>
            </Border>
        </Box>
    );
};
