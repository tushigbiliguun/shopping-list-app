import React, { createContext, useContext } from 'react'

export const colors = {
    white: '#FFFFFF',
    offwhite: '#F8F8F8',
    gray: '#CCCCCC',
    black: '#000000',
    primary1: '#5856D6',
    primary2: '#F43426',
    primary3: '#007AFF',
    primary4: '#4CD964',
    secondary1: '#047C52',
    secondary2: '#FFE200',
    secondary3: '#64D2FF',
    secondary4: '#261BAC'
};

export const DEFAULT = {
    baseSpace: 4,
    colors
}

export const ThemeContext = createContext(DEFAULT)

export const ThemeProvider: React.FC<any> = ({ children }) => {
    return (
        <ThemeContext.Provider value={DEFAULT}>{children}</ThemeContext.Provider>
    )
}

export const useTheme = () => useContext(ThemeContext)