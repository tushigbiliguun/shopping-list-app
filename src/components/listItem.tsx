import React, { useState } from 'react';
import { Box, Text, Border, Spacing, Button } from './main'
import { TouchableOpacity } from 'react-native-gesture-handler';
import CheckBox from '@react-native-community/checkbox';
import { TrashIcon } from './icons';

export const ListItem: React.FC<any> = ({ item, deleteItem }) => {

    const [toggleCheckBox, setToggleCheckBox] = useState(false)
    const [restore, setRestore] = useState(false)

    return (
        <TouchableOpacity disabled={toggleCheckBox === false ? false : true}>
            <Spacing margintop={2}>
                <Border radius={10}>
                    <Box width={'100%'} height={50} role={'offwhite'} flexDirection={'row'} alignItems={'center'} justifyContent={'space-evenly'}>
                        <Box flex={1}>
                            <Spacing padleft={4}>
                                <CheckBox
                                    disabled={false}
                                    onAnimationType={'one-stroke'}
                                    offAnimationType={'one-stroke'}
                                    onTintColor={'#5856D6'}
                                    onCheckColor={'#5856D6'}
                                    value={toggleCheckBox}
                                    onValueChange={(newValue) => setToggleCheckBox(newValue)}
                                />
                            </Spacing>
                        </Box>
                        <Box flex={4}>
                            <Spacing marginleft={3}>
                                {
                                    toggleCheckBox === false &&
                                    <Text>{item.text}</Text>
                                }
                                {
                                    toggleCheckBox === true &&
                                    <Text role={'gray'}>{item.text}</Text>
                                }
                            </Spacing>
                        </Box>
                        <Box flex={0.75}>
                            {
                                toggleCheckBox === false &&
                                <Text>{item.amount}</Text>
                            }
                            {
                                toggleCheckBox === true &&
                                <Text role={'gray'}>{item.amount}</Text>
                            }
                        </Box>
                        <Box flex={1}>
                            <Button category={'fill'} type={'secondary'} width={40} height={30} onPress={() => deleteItem(item.key)}>
                                <TrashIcon role={'white'} height={22} width={15} />
                            </Button>
                        </Box>
                    </Box>
                </Border>
            </Spacing>
        </TouchableOpacity>
    )
}