export type ColorType = 
    | 'white'
    | 'offwhite'
    | 'gray'
    | 'black'
    | 'primary1'
    | 'primary2'
    | 'primary3'
    | 'primary4'
    | 'secondary1'
    | 'secondary2'
    | 'secondary3'
    | 'secondary4'

export type IconType = {
    role?: ColorType;
    width?: number;
    height?: number;
}
